import React from 'react';

import { Todo } from '../core/entities';
import { todoService } from '../services';

export const useTodos = () => {
  const [todos, _setTodos] = React.useState<Todo[]>([]);

  React.useEffect(() => {
    todoService.getTodos().then(todos => _setTodos(todos));
  }, []);

  return { todos };
};
