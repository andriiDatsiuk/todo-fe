import { Todo } from '../../core/entities';
import { ITodoService } from '../../core/interactors';

import { ApiClient } from '../common';

export class TodoService implements ITodoService {
  constructor(private apiClient: ApiClient) {}

  public async getTodos(): Promise<Todo[]> {
    const res = await this.apiClient.get('v1/todo/todos');
    const json = await res.json();

    return json;
  };
};
