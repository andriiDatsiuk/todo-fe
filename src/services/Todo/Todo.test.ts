import { TodoService } from "./Todo";


const todosMock = [
  {
    _id: '1',
    title: 'Test todo 1',
    completed: false
  },
  {
    _id: '2',
    title: 'Test todo 2',
    completed: true
  },
];

const apiClientMock = {
  get: jest.fn(),
  post: jest.fn(),
  put: jest.fn(),
  delete: jest.fn(),
  setHeader: jest.fn(),
  deleteHeader: jest.fn(),
};

describe('Todo service', () => {
  describe('getTodos', () => {
    test('Should return Todo[]', async () => {
      const todoService = new TodoService(apiClientMock);
      const responseMock = {
        json: jest.fn().mockResolvedValueOnce(todosMock)
      };
      apiClientMock.get.mockResolvedValueOnce(responseMock);

      const res = await todoService.getTodos();

      expect(res).toMatchObject(todosMock);
      expect(responseMock.json).toBeCalled();
    })
  })
})