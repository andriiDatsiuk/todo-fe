import { apiService } from './common';
import { TodoService } from './Todo';

export const todoService = new TodoService(apiService);
