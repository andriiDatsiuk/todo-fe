export interface Logger {
  warn(message: string): void
  error(message: Error | string): void
  debug(message: Error | string): void
}

export default class ConsoleLogger implements Logger {
  public warn(message: string): void {
    console.warn(message)
  }

  public error(message: string | Error): void {
    console.error(message)
  }

  public debug(message: string | Error): void {
    console.debug(message)
  }
}
