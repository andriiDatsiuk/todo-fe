import { AppError, CriticalError } from '../../core/errors'

import { Logger } from './Logger'

interface IEncodedObject {
  [key: string]: string
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface IApiConfig<T extends Record<string, unknown> = any>
  extends Omit<RequestInit, 'body'> {
  body?: T | RequestInit['body']
  type?: 'urlencoded' | 'json' | 'form'
  /** skipAuth is intented to prevent authorization header sending if it is present */
  skipAuth?: true
}

type ApiMethod = (path: string, config?: IApiConfig) => Promise<Response>

export interface ApiClient {
  get: ApiMethod
  post: ApiMethod
  put: ApiMethod
  delete: ApiMethod
  setHeader(key: string, value: string): void
  deleteHeader(key: string): void
}

interface ApiErrorBody {
  code: string
  message: string
  status: number
  timestamp: string
}

type MethodType = 'get' | 'post' | 'put' | 'delete'

export class ApiError extends AppError {
  public status: number

  public constructor(response: Response, errorCode?: string | null, message?: string) {
    const code: string = errorCode ? errorCode.toLowerCase() : 'api_error'

    super(code, message)
    this.status = response.status
  }
}

const isNetworkError = (err: unknown) =>
  err && err instanceof TypeError && err.message === 'Network request failed'

const isAuthorizationError = (res: Response) => res.status === 403

export default class WebApiClient implements ApiClient {
  private static methods: MethodType[] = ['get', 'post', 'put', 'delete']

  public get!: ApiMethod
  public post!: ApiMethod
  public put!: ApiMethod
  public delete!: ApiMethod

  private headers: Headers = new Headers({ 'Content-Type': 'application/json' })

  public constructor(private apiHost: string, private logger: Logger) {
    WebApiClient.methods.forEach(this.createMethod)
  }

  public setHeader(key: string, value: string): void {
    this.headers.set(key, value)
  }

  public deleteHeader(key: string): void {
    this.headers.delete(key)
  }

  private createMethod = (method: MethodType) => {
    const newMethod: ApiMethod = async (path, config = {}) => {
      try {
        const { body, ...compatibleConfig } = config
        const requestConfig: RequestInit = { ...compatibleConfig, method }

        requestConfig.headers = this.prepareHeaders(config)

        if (body) {
          requestConfig.body = this.prepareBody(config)
        }

        const url = this.formatUrl(path)
        const response = await fetch(url, requestConfig)

        if (!response.ok) {
          const error = await this.prepareError(response)

          throw error
        }

        return response
      } catch (e) {
        let err = e

        if (isNetworkError(err)) {
          err = new CriticalError('network_failed_error')
        }

        throw err
      }
    }

    this[method] = newMethod
  }

  private prepareHeaders(config: IApiConfig): Headers {
    // Create current call headers from the default ones
    const currentHeaders = new Headers(this.headers)

    if (config.headers) {
      const providedHeaders = new Headers(config.headers)

      // Apply provided headers and in case override default ones
      for (const [key, value] of providedHeaders.entries()) {
        currentHeaders.set(key, value)
      }
    }

    if (config.type === 'json') {
      currentHeaders.set('Content-Type', 'application/json')
    }

    if (config.type === 'urlencoded') {
      currentHeaders.set('Content-Type', 'application/x-www-form-urlencoded')
    }

    // In case form data is passed, the content type header should be taken from it.
    // So we need to clear any content type taken from the default headers or from the passed ones.
    if (config.type === 'form' && config.body instanceof FormData) {
      currentHeaders.delete('Content-Type')
    }

    if (config.skipAuth && currentHeaders.has('Authorization')) {
      currentHeaders.delete('Authorization')
    }

    return currentHeaders
  }

  private prepareBody({ body, type }: IApiConfig): RequestInit['body'] {
    if (!body) {
      return null
    }

    if (this.isBodyCustomObj(body)) {
      if (type === 'urlencoded') {
        const encoded = this.encodeBody(body)

        return new URLSearchParams(encoded).toString()
      } else {
        return JSON.stringify(body)
      }
    } else {
      return body
    }
  }

  private async prepareError(response: Response): Promise<ApiError | CriticalError> {
    this.logger.debug(`Preparing an API error response: ${response}`)

    const authErr = await this.prepareAuthorizationError(response)

    return authErr || this.prepareApiError(response)
  }

  private async prepareAuthorizationError(response: Response): Promise<CriticalError | null> {
    return isAuthorizationError(response) ? new CriticalError('auth_failed_error') : null
  }

  private async prepareApiError(response: Response): Promise<ApiError> {
    let message: string | undefined = undefined
    let errorCode: string | null = null

    try {
      const json: ApiErrorBody = await response.json()

      message = json.message
      errorCode = json.code
    } catch (e) {
      this.logger.warn(e.message)
    }

    return new ApiError(response, errorCode, message)
  }

  private isBodyCustomObj(body: IApiConfig['body']): body is Record<string, string> {
    return (
      typeof body === 'object' &&
      !(body instanceof ArrayBuffer) &&
      !(body instanceof FormData) &&
      !(body instanceof URLSearchParams)
    )
  }

  private encodeBody(body: Record<string, string>): IEncodedObject {
    return Object.entries(body).reduce<IEncodedObject>((encoded, [key, value]) => {
      encoded[key] = encodeURIComponent(value)

      return encoded
    }, {})
  }

  private adjustPath = (path: string): string => (path[0] !== '/' ? `/${path}` : path)

  private formatUrl = (path: string): string => {
    const adjustedPath = this.adjustPath(path)
    return this.apiHost + adjustedPath
  }
}
