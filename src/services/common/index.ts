import ApiClient from './ApiClient';
import Logger from './Logger';

import config from '../../config';

export const loggerService = new Logger();
export const apiService = new ApiClient(config.api.url, loggerService);

export * from './ApiClient';
export * from './Logger';
