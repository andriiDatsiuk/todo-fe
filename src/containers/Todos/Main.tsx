import React from 'react';

import { useTodos } from '../../hooks';

interface ITodosProps {}

export const Todos: React.FC<ITodosProps> = () => {

  const { todos } = useTodos();

  return (
    <div>
      Todos page
    </div>
  );
}
