import { TodoInteractor } from './Todo';

const todosMock = [
  {
    _id: '1',
    title: 'Test todo 1',
    completed: false
  },
  {
    _id: '2',
    title: 'Test todo 2',
    completed: true
  },
];

const mock = {
  getTodos: jest.fn().mockResolvedValueOnce(todosMock)
};

describe('Todo interactor', () => {
  describe('getTodos', () => {
    test('Should fetch Todos', async () => {
      const todoInteractor = new TodoInteractor(mock);
      const res = await todoInteractor.getTodos();

      expect(res).toBe(todosMock);
      expect(mock.getTodos).toHaveBeenCalled();
    })
  })
})