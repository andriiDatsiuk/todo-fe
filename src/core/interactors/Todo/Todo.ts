import { Todo } from "../../entities";

export interface ITodoService {
  getTodos(): Promise<Todo[]>
}

export class TodoInteractor {
  public constructor(private todoService: ITodoService) {}

  public getTodos(): Promise<Todo[]> {
    return this.todoService.getTodos();
  }
}