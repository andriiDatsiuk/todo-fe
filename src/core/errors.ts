export class AppError extends Error {
  public code: string | undefined | null

  constructor(code?: string | null, message?: string) {
    super(message)

    this.code = code
    this.name = this.constructor.name
  }
}

export class ValidationError extends AppError {}

export class CriticalError extends AppError {}
