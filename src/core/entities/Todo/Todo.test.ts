import { ITodoData, Todo } from './Todo';

import { ValidationError } from '../../errors'

describe('Todo entity', () => {
  describe('constructor()', () => {
    test('should fail with invalid data provided', () => {
      try {
        new Todo({} as ITodoData);
      } catch (e) {
        expect(e).toBeInstanceOf(ValidationError);
        expect(e).toHaveProperty('code', 'invalid_todo_data');
      }
    })

    test('should create todo', () => {
      const todoData = {
        _id: '1',
        title: 'Test todo',
        completed: true
      };
      const todo = new Todo(todoData);

      expect(todo).toBeTruthy();
      expect(todo).toHaveProperty('_id');
      expect(todo).toHaveProperty('title');
    })
  })
})
