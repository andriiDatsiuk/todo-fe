import { ValidationError } from '../../errors'

export interface ITodoData {
  _id: string;
  title: string;
  completed: boolean;
}

export class Todo {
  public _id: string;
  public title: string;
  public completed: boolean;

  public constructor(todo: ITodoData) {
    if (isTodoDataValid(todo)) {
      this._id = todo._id;
      this.title = todo.title;
      this.completed = todo.completed;
    } else {
      throw new ValidationError('invalid_todo_data')
    }
  }
}

const isTodoDataValid = (todo: ITodoData) =>
  todo.hasOwnProperty('_id') && typeof todo['_id'] === 'string' &&
  todo.hasOwnProperty('title') && typeof todo['title'] === 'string';
