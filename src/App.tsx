import * as React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import { Todos } from './containers';

export const App: React.FC<{}> = () => {
  return (
    <div>
      <Router>
        <Switch>
          <Route path="/todos" component={Todos} />
          <Redirect to="/todos" />
        </Switch>
      </Router>
    </div>
  );
}
